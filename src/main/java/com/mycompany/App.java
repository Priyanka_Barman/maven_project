package com.mycompany;

import com.mycompany.demo123.Card;
import com.mycompany.demo123.Player;
import com.mycompany.demo123.Rank;

import java.util.UUID;

public class App {
    public static void main(String[] args) {

        Player player = Player.builder().build();
        player.setPlayerId(UUID.randomUUID().toString());
        player.setPlayerName("John");
        player.setPlayerRank(Rank.LOWER);
        player.setCard(Card.HEARTS);
        System.out.println(player.getDetails());

    }
}

