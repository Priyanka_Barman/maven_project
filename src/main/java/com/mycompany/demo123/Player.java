package com.mycompany.demo123;

import lombok.*;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Locale;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    private String playerId;
    private String playerName;
    private Rank playerRank;
    private Card card;
    public String getDetails()  {
      /*  String str=new String();
        Formatter formatter = new Formatter(str);*/
        return String.format("Player_ID: %s \nPlayer_Name: %s \nPlayer_Rank: %s \nCard_Details: %s \n",playerId,playerName,playerRank.getPlayerRank(),card.getCardRank());
    }


}

